<!-- http://localhost/b168/s03/a1/index.php -->
<?php require_once "./code.php"; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Activity 2</title>
</head>
<body>

	<h1>Objects from Variables</h1>
	<p><?php echo $buildingObj->name ?></p>
	<p><?php echo $buildingObj->address->country ?></p>

	<h1>Objects and Classes</h1>
	<p><?php var_dump($building); ?></p>

	<h1>Inheritance (Condimium Object)</h1>
	<p><?php echo $condominium->name; ?></p>
	<p><?php echo $condominium->floors; ?></p>
	<p><?php echo $condominium->address; ?></p>

	<h1>Polymorphism (Changing of printName Behavior</h1>
	<p><?php echo $condominium->printName(); ?></p>

</body>
</html>